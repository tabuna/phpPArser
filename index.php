<?php
include_once 'vendor/autoload.php';

use DiDom\Document;
use GuzzleHttp\Client;

use SimpleExcel\SimpleExcel;

$excel = new SimpleExcel('CSV');
$excel->parser->loadFile('test.csv');




$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'https://2gis.ru/lipetsk/firm/',
    // You can set any number of default request options.
    'timeout'  => 1.0,
]);

$i = 7881827628877000;

while ($i < 7881827628877380) {

    try {

        $organization = [];


        $test = $client->get($i);
        $string = $test->getBody()->getContents();
        $document = new Document($string);

        if (count($elements = $document->find('.cardHeader__headerNameText')) == 0) {
            $i++;
            continue;
        }


        /**
         * Название компании
         */
        $name = $document->find('.cardHeader__headerNameText');
        foreach ($name as $nam) {
            array_push($organization,$nam->text());
           // $nam->text();
            //echo $nam->html() . "<br>";
        }


        /**
         * Адреса
         */
        $adress = $document->find('.firmCard__addressStreet');
        $Oadres = '';
        foreach ($adress as $adres) {
            $Oadres .= strip_tags($adres->text());
           // echo $adres->html() . "<br>";
        }
        array_push($organization,$Oadres);


        /**
         * Веб-сайт
         */
        $website = $document->find('.contact__linkText');
        $Owebsite = "";
        foreach ($website as $web) {
            $Owebsite .= $web->text();
            //echo $web->html() . "<br>";
        }
        if($Owebsite == "")
        {
            $Owebsite = "-";
        }

        array_push($organization,$Owebsite);

        //echo "<hr><br>";

        /**
         * Email
         */

        $email = $document->find('._type_email');
        $Oemail = '';
        foreach ($email as $mail) {
            $Oemail .= $mail->text();
            //echo $mail->html() . "<br>";
        }
        if($Oemail == "")
        {
            $Oemail = "-";
        }
        array_push($organization,$Oemail);


        //echo "<hr><br>";

        /**
         * Номера телефона
         */
        $phones = $document->find('._type_phone');
        $Ophones = "";
        foreach ($phones as $phone) {
            $Ophones .= $phone->text();
          //  echo $phone->html() . "<br>";
        }
        array_push($organization,$Ophones);

        $i++;
        $excel->writer->addRow($organization);
    }
    catch (Exception $error)
    {
        $i++;
    }
}

$excel->writer->saveFile('2gis');
